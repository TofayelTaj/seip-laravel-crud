<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description')->nullable();
            $table->timestamps();
            $table->bigInteger('price')->default(0);
            $table->bigInteger('quantity')->default(0);
            $table->string('unit')->default(0);
<<<<<<< HEAD
=======
            $table->softDeletes();
>>>>>>> f0ad92e022a784e2736bc23379fa70f8e057b784
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
