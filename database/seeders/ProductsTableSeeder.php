<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        \App\Models\Product::create([
            'title' => 'sony',
            'price' => 12,
            'quantity' => 12,
            'unit' => 32,
            'description' => 'Fashion Description',
        ]);
        \App\Models\Product::create([
            'title' => 'sony',
            'price' => 12,
            'quantity' => 12,
            'unit' => 32,
            'description' => 'Fashion Description',
        ]);
        \App\Models\Product::create([
            'title' => 'sony',
            'price' => 12,
            'quantity' => 12,
            'unit' => 32,
            'description' => 'Fashion Description',
        ]);
    }
}
