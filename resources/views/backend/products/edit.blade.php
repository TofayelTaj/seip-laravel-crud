<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Edit Form
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Categories </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Edit</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Edit Category <a class="btn btn-sm btn-info" href="{{ route('products.index') }}">List</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{ route('products.update', ['product' => $product->id]) }}" method="post">
                @csrf
                @method('patch')

                <x-backend.form.input name="title" :value="$product->title"></x-backend.form.input>
                <x-backend.form.input name="price" type="number" :value="$product->price"></x-backend.form.input>
                <x-backend.form.input name="quantity" type="number" :value="$product->quantity"></x-backend.form.input>
                <x-backend.form.input name="unit" type="number" :value="$product->unit"></x-backend.form.input>
                <x-backend.form.textarea name="description"  :value="$product->description"></x-backend.form.input>

             

                <div class="form-floating mb-3 mb-md-0">
                    <input name="price" class="form-control" id="inputTitle" type="number" placeholder="Enter your price" value="{{ old('price', $product->price) }}">
                    <label for="inputTitle">price</label>
                    @error('price')
                    <span class="small text-danger">{{ $message }}</span>
                    @enderror

                </div>
                <div class="form-floating mb-3 mb-md-0">
                    <input name="quantity" class="form-control" id="inputTitle" type="number" placeholder="Enter your quantity" value="{{ old('quantity', $product->quantity) }}">
                    <label for="inputTitle">quantity</label>
                    @error('quantity')
                    <span class="small text-danger">{{ $message }}</span>
                    @enderror

                </div>
                <div class="form-floating mb-3 mb-md-0">
                    <input name="unit" class="form-control" id="inputTitle" type="number" placeholder="Enter your unit" value="{{ old('unit',$product->unit) }}">
                    <label for="inputTitle">unit</label>
                    @error('unit')
                    <span class="small text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="form-floating mt-3">
                    <textarea name="description" class="form-control" id="inputDescription" placeholder="Description">
                    {{ old('description', $product->description) }}
                    </textarea>
                    <label for="inputDescription">Description</label>

                    @error('description')
                    <span class="small text-danger">{{ $message }}</span>
                    @enderror

                </div>
              

                <div class="mt-4 mb-0">
                    <button type="submit" class="btn btn-primary">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>


</x-backend.layouts.master>