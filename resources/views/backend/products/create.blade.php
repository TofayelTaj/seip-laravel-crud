<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Add Form
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Products </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Add New</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Create Products <a class="btn btn-sm btn-info" href="{{ route('products.index') }}">List</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('products.store') }}" enctype="multipart/form-data" method="post">
                @csrf

                <x-backend.form.php name="title"></x-backend.form.php>
                <x-backend.form.php name="price" type="number"></x-backend.form.php>
                <x-backend.form.php name="quantity" type="number"></x-backend.form.php>
                <x-backend.form.php name="unit" type="number"></x-backend.form.php>
                <x-backend.form.testarea name="description"></x-backend.form.testarea>
                <div class="mt-4 mb-0">
                    <x-backend.form.button>Submit</x-backend.form.button>
                </div>

                
            </form>
        </div>
    </div>


</x-backend.layouts.master>
