@props(['name'])
     <div class="form-floating mt-3">
        <textarea name="{{name}}" class="form-control" id="{{name}}" placeholder="{{name}}">
        {{ old('name') }}
        </textarea>
        <label for="inputDescription">{{ucword($name)}}</label>

        @error('name')
        <span class="small text-danger">{{ $message }}</span>
        @enderror

    </div>