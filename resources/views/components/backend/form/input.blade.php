
@porp(['name'])
<div class="form-floating mb-3 mb-md-0">
    <input name="{{name}}" class="form-control" id="inputTitle" type="text" placeholder="Enter your title" value="{{ old('title') }}">
    <label for="inputTitle">{{name}}</label>
    @error('{{name}}')
    <span class="small text-danger">{{ $message }}</span>
    @enderror

</div>