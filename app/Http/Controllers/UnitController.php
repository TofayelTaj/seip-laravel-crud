<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Http\Requests\UnitRequest;
use App\Models\Category;
use App\Models\Unit;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UnitController extends Controller
{
    public function index()
    {
        $unit = Unit::latest()->get();
        // dd($categories);
        return view('backend.unit.index', [
            'units' => $unit
        ]);
    }

    public function create()
    {
        return view('backend.unit.create');
    }

    public function store(UnitRequest $request)
    {

        try {
            // $request->validate([
            //     'title' => 'required|min:3|max:50|unique:categories,title',
            //     // 'title' => ['required', 'min:3', Rule::unique('categories', 'title')],
            //     'description' => ['required', 'min:10'],
            // ]);
            // $imageName = request()->file('image')->store('storage/app/public/images');
            // dd($imageName);
            Unit::create([
                'title' => $request->title,
              
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('units.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    // public function show(Category $id)
    public function show(Unit $unit)
    {
        // $category = Category::where('id', $id)->firstOrFail();
        // $category = Category::find($id);
        return view('backend.unit.show', [
            'unit' => $unit
        ]);
    }

    public function edit(Unit $unit)
    {
        return view('backend.unit.edit', [
            'unit' => $unit
        ]);
    }

    public function update(UnitRequest $request, Unit $unit)
    {
        try {
            // $request->validate([
            //     // 'title' => 'required|min:3|max:50|unique:categories,title,'.$category->id,
            //     'title' => ['required', 'min:3', 
            //         Rule::unique('categories', 'title')->ignore($category->id)
            //     ],
            //     'description' => ['required', 'min:10'],
            // ]);

            $unit->update([
                'title' => $request->title,
              
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('units.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    public function destroy(Unit $unit)
    {
        try {
            $unit->delete();
            return redirect()->route('units.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }
}
