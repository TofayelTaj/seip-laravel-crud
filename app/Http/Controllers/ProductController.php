<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    public function index()
    {
        $product = Product::latest()->get();
        // dd($categories);
        return view('backend.products.index', [
            'products' => $product
        ]);
    }

    public function create()
    {
        return view('backend.products.create');
    }

    public function store(ProductRequest $request)
    {

        try {
<<<<<<< HEAD
            // $request->validate([
            //     'title' => 'required|min:3|max:50|unique:categories,title',
            //     // 'title' => ['required', 'min:3', Rule::unique('categories', 'title')],
            //     'description' => ['required', 'min:10'],
            // ]);
            // $imageName = request()->file('image')->store('storage/app/public/images');
            // dd($imageName);
=======
           
>>>>>>> f0ad92e022a784e2736bc23379fa70f8e057b784
            Product::create([
                'title' => $request->title,
                'description' => $request->description,
                'price' => $request->price,
                'unit' => $request->unit,
                'quantity' => $request->quantity,
            ]);

<<<<<<< HEAD
            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('products.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    // public function show(Category $id)
    public function show(Product $product)
    {
        // $category = Category::where('id', $id)->firstOrFail();
        // $category = Category::find($id);
=======
            return redirect()->route('products.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }


    public function show(Product $product)
    {
       
>>>>>>> f0ad92e022a784e2736bc23379fa70f8e057b784
        return view('backend.products.show', [
            'product' => $product
        ]);
    }

    public function edit(Product $product)
    {
        return view('backend.products.edit', [
            'product' => $product
        ]);
    }

    public function update(ProductRequest $request, Product $product)
    {
        try {
<<<<<<< HEAD
            // $request->validate([
            //     // 'title' => 'required|min:3|max:50|unique:categories,title,'.$category->id,
            //     'title' => ['required', 'min:3', 
            //         Rule::unique('categories', 'title')->ignore($category->id)
            //     ],
            //     'description' => ['required', 'min:10'],
            // ]);
=======
           
>>>>>>> f0ad92e022a784e2736bc23379fa70f8e057b784

            $product->update([
                'title' => $request->title,
                'description' => $request->description,
                'price' => $request->price,
                'unit' => $request->unit,
                'quantity' => $request->quantity,
            ]);

<<<<<<< HEAD
            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('products.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
=======
            return redirect()->route('products.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
>>>>>>> f0ad92e022a784e2736bc23379fa70f8e057b784
        }
    }

    public function destroy(Product $product)
    {
        try {
            $product->delete();
            return redirect()->route('products.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            
        }
    }
<<<<<<< HEAD
=======

    public function trash(){
        $trash = Product::onlyTrashed()->get();
        return view('backend.products.trash', ['products' => $trash]);
    }

    public function restore($id){
      
        $product = Product::onlyTrashed()->findOrFail($id)->restore();
        return redirect()->back();
    }

    public function delete($id){
        $product = Product::onlyTrashed()->findOrFail($id)->forceDelete();
        return redirect()->back();
    }

>>>>>>> f0ad92e022a784e2736bc23379fa70f8e057b784
}
